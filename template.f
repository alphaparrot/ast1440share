c
c Compilation:
c
c gfortran -ffixed-form -O3 -fopenmp -ffixed-line-length-72 -cpp -Wall 
c    -ffpe-trap=zero,invalid,overflow -fbacktrace -fbounds-check 
c    -finit-local-zero -o template template.f
c
c -ffixed-line-length-72 can be omitted if sticking with 72, but to
c change the line length limit, change to something like
c -ffixed-line-length-137 or whatever floats your boat.
c
c                      FORTRAN TEMPLATE FILE
c
c This file will demonstrate some Fortran idioms, in the old 77 style.
c I will add to it as I think of things--it should hopefully include
c enough to give you all you need for the assignment.
c
c      COMMENTS
c
c As you can guess, comments are started with a 'c' in the first
c column.
         ! You can also include in-line comments with a bang (!).
c
c 
c--------------------------------------------------------------------72
c                       COLUMNS IN FORTRAN
c
c Normal code begins in the 7th column. Unless specified otherwise
c in the compiler arguments, you're limited to 72 columns. If you want
c to split a line, simply put an ampersand (&) in the 6th column. You
c can place numbers in the first 5 columns to bookmark statements or 
c format declarations (i.e. if you define a format for printing once,
c and want to re-use it, you might assign it a numerical tag, and then
c just to refer to it with that). You can comment out a line by putting
c a 'c' in the first column.
c
c Fortran does not care about indentation. You should indent the bodies
c of conditional clauses and loops for readability, and always use 
c spaces, not tabs.
c
c NEVER USE GOTO STATEMENTS.
c
c Here we go!
c
c Lines with a hash (#) at the beginning of the line are called
c "preprocessor statements". These are evaluated by the compiler,
c and don't appear in the compiled code. So for example, if we
c define 'small' in a preprocessor statement, then everywhere it
c appears in the code, the compiler will replace it with 0.000001.
#define small 0.000001
#define somevar1 1
#define somevar2 17
#define NSIZE 256
#define NX 8
#define NY 32

#define ifwrite 1

c PI isn't defined by default in fortran, and there aren't standard
c math built-ins to get it from. So we define it for ourselves. Give
c yourself a fair number of digits!
#define pi 3.1415926535897932

    
c--------------------------------------------------------------------72
c              GETTING STARTED

      program template !This declares the main program
      
c Declarations are the very first thing in your program, and have
c to happen here. By default, you don't need to specify type. That's
c implicitly done, meaning that variables starting with i-n are 
c integers, and variables starting with any other letter are reals
c (4-byte floats, though you can declare 8-byte doubles with real*8).
c If you don't want this and want to specify the type of every variable
c like in C, then include 'implicit none' at the beginning of the 
c program.
      
      parameter (nsteps=8) !parameters are basically constants.
      
      dimension dd(0:NSIZE+1)
      dimension output(NSIZE)
c We've just declared two arrays, dd and output. dd is indexed from
c 0 to NSIZE+1, and output is indexed from 1 to NSIZE. Note that
c due to implicit typing, both will be filled with reals. If we had
c declared koutput instead of output, it'd be filled with ints. Indexes
c can be in whatever range you want--we could have instead written
c dimension dd(-NSIZE/2:NSIZE/2). This doesn't affect memory layout,
c just what the integer index range is defined as (specifically, in
c the above example, -NSIZE/2 would be mapped to 0, and NSIZE/2 to 
c NSIZE+1).
c

      dimension tile(0:NY,0:NX)
c We've just declared a 2D array. Fortran is column-major, meaning
c the columns (y-axis) are fastest varying. Put another way, items
c next to each other in memory appear in different rows but in the
c same column. This means that rather than the inner loop changing
c the second index, it should change the first index. 

      integer pstep !Demonstration of integer declaration
      real nuvar !Demonstration of real declaration
      
      character sname*13 !Declare a 13-element character string
      
    
c--------------------------------------------------------------------72
c         PLAYING AROUND WITH CONDITIONALS AND PRINTING   
      
      print *,'Hello, world!'
      print *,'These are print statements.'
      print *,'The main program now continues!'
      print *,''

c Below is a preprocessor if statement. If somevar == 1, the code
c that follows will be compiled, otherwise it will be excluded.
c Remember that this branching happens at compile-time, not at 
c run-time.
#if somevar1 == 1

      if (somevar2 .eq. 0) print *,'If-statements can be 1-line.'
      if (somevar2 .gt. 0) then
         !This is an if-statement that will have more than 1 line.
         !comparison operators include .gt., .ge., .lt., .le., .eq.,
         !and .ne.. Logicals are .TRUE., .FALSE., .NOT., .AND., and
         !.OR.. Comparing booleans is with .EQV. and .NEQV.. .NEQV. is
         !really just an XOR. 
         
        
         print *,'I have gained',somevar2,'apples.'
      else
         print *,'I have lost',somevar2,'apples.'
      endif !Remember your endifs!
      
#else

      print *,'WTF is an apple?'
      
#endif
c To recap, if somevar == 1, then the if clause will be compiled.
c If somevar does not equal 1, then the else clause will be compiled.
    
    
c--------------------------------------------------------------------72
c              DEFINE OUTPUT FILES
#if ifwrite == 1
      open(unit=32,file='templateout.txt',status='replace')
      open(unit=33,file='writelog.txt',position='append',
     &     status='unknown') !Note continuation character in column 6.
c The unit numbers are arbitrary. Generally good to avoid 0-10, since
c those are often reserved for things like writing back to the terminal

1121  format (//'    i',8x,'input',8x,'output'//)
1122  format (i5,2x,1p8e13.5)
1123  format (1p8e13.5)

c That just declared two formats. The first explicitly provides for 
c a line that starts with 4 spaces than 'i', is followed by 8 spaces,
c the word 'input', 8 more spaces, and then 'output'. The second
c defines a line with a 5-column-wide integer, 2 spaces, and a number
c in scientific notation, with 8 columns for the number, a non-zero 
c digit before the decimal, 5 digits included to the right of the 
c decimal, and 13 columns allocated overall. The number given in the 
c tag (first 4-5 columns) is completely arbitrary.

      write (33,*) '        LOG FILE' !Unformatted write
      write (32,*) '     OUTPUT FILE'
      write (33,*) !blank line
      write (33,1121) !Write the format defined in tag 1121.
      
#endif

    
c--------------------------------------------------------------------72
c              START DOING THINGS

      !A simple loop:
      do i=0,NSIZE+1 !i is implicitly declared as an integer
         dd(i) = i + (i**1.1)/sqrt(i+1.0)
     &          -sin(i*pi/10.0)
         dd(i) = dd(i) + asin(0.5)*180.0/pi !asin = arcsin
         
#if ifwrite==1
 ! This clause only included if we specifically said so.
         write(33,1122) i,dd(i)
#endif
         
      enddo

c	Some parallelization: OpenMP can handle the thread
c	load-balancing for you, and split up the work. Just
c	tell it to do a loop in parallel. You can declare some
c	variables as private, meaning each thread has its own
c	copy.
c
c	You'll need to specify the number of threads externally,
c	by setting it as an environment variable. For Bash, the 
c	command will be export OMP_NUM_THREADS=4, where 4 is the
c	number of threads available (on my machine it's 8). It may
c	be worthwhile to put this in your .bashrc or .bash_profile.
c
!$OMP PARALLEL DO PRIVATE(i)
      do i=0,NSIZE+1
         dd(i) = dd(i) + 2
      enddo
!$OMP END PARALLEL DO

      kount = 0
!$OMP PARALLEL DO PRIVATE(i)
      do i=0,NSIZE+1
        dd(i) = dd(i) - 1
c		Each thread will attempt to increment kount,
c		possibly leading to race conditions. To avoid
c		this, tell OpenMP to make it an atomic operation:
c		only one thread can touch it at once. This is a
c		slowdown in your code, but with relatively little
c		overhead. To avoid this slowdown, avoid writing
c		your code like that in the first place. Sometimes
c		it's unavoidable.
!$OMP ATOMIC
        kount = kount + 1
      enddo
!$OMP END PARALLEL DO

      print *,kount
      
      
      pstep = 16
      nuvar = dd(pstep)
     
      write (sname,'(1p8e13.5)') nuvar !Write nuvar to sname with 2 digits
      
      do n=1,nsteps
         ! Call a subroutine:
         call mysubroutine1(dd,NSIZE+2,pstep,n,output,NSIZE)
      enddo
   
c--------------------------------------------------------------------72
c              PRINT AND WRITE THINGS
      
      print *,''
      
      do i=1,NSIZE
         print *,sname,i,output(i)
#if ifwrite==1
         write (32,1122) i,dd(i),output(i)
         write (33,1122) i,dd(i),output(i)
#endif         
      enddo
      print *,''
      
      do i=NSIZE,1,-2 
c      Start the loop at i=NSIZE, and count down in steps of 2 until 1.
         print *,i,output(i)
      enddo
      
#if ifwrite==1
      write (32,*)
      write (33,*) '------BREAK------'
      write (33,*)
      close(32)
#endif
      
   
c--------------------------------------------------------------------72
c              USE THAT 2-D ARRAY
      
c	If you have nested loops that are perfectly rectangular
c	and dependence-free, you can tell OpenMP to basically
c	collapse them into just one loop, which parallelizes better.
c	This is done by specififying COLLAPSE(2).
!$OMP PARALLEL
!$OMP DO PRIVATE(i,j) COLLAPSE(2)
      do i=1,NX
         do j=1,NY
            tile(j,i) = output((i-1)*NY + j)
         enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
      
      do i=1,NX
         do j=1,NY
            write (*,1123,advance='no') tile(j,i)
            write (33,1123,advance='no') tile(j,i)
            ! Giving write * as the unit number writes to terminal.
            ! Specifying advance='no' avoids line-breaks.
         enddo
         write (33,*)
         write (*,*) !Advance the line.
      enddo
      
      end !End the program.
      
      
   
c====================================================================72
c              A SUBROUTINE!
      
      subroutine mysubroutine1(data,nn1,interval,iter,
     &                         outd,nn2)
     
      dimension data(nn1), outd(nn2)
      
      character si*2
      
      length = (nn1-iter)/interval - 1
      
      write(si,'(I2.2)') iter
      
      do k=1,length
         outd(k*interval) = log(data(k*interval+iter)*exp(iter*0.5))
      enddo
      
c	Write a binary ('unformatted') file that can be opened by
c	NumPy:      
      open(unit=18,file='intermed'//si//'.dat',form='UNFORMATTED',
     &     access='DIRECT',recl=nn2*4) !recl=length*4 means the 
                                       !file's record length should
                                       !be 4 bytes times nn2.
c	Two slashes '//' concatenate character strings.                                       
      write(18,rec=1) outd
      close(18)

c Note that we're not actually returning anything. We can write our
c output directly to one of the variables we passed as arguments.
      return
      
      end
      
      
      
        
      
      
      
         
      
      